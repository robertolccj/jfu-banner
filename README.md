# JFU Banner

Modelo LaTeX para banner da [Jornada de Física 2019](https://jfu.vpeventos.com).



### Como usar?

Se você já possui o LaTeX instalado no seu computador, faça aqui o [download do template](https://gitlab.com/robertolccj/jfu-banner/-/archive/master.zip).

Caso queira, também pode edite online no [Overleaf](https://www.overleaf.com/docs?snip_uri=https://gitlab.com/robertolccj/jfu-banner/-/archive/master.zip).



### Vídeo Tutorial

Tenha uma noção de como funciona o Overleaf neste [vídeo tutorial](https://scitech.video/videos/watch/93165975-5e0d-4b61-9f45-b5ac3eb12d4a)



### Atenção

Para a geração da bibliografia, este template utiliza o pacote [Biber](https://www.ctan.org/pkg/biber).
Possui compatibilidade com o [Bibtex](https://pt.wikipedia.org/wiki/BibTeX). Assim, no arquivo de bibliografia (bibliografia.bib) você pode usar caracteres UTF8 (acento, cedilha, etc).


---
FIM
